#!/bin/bash

set -euo pipefail 

tmp_folder="./tmp-classes"

javac -proc:none -cp ServerPythonInterpreterPlugin/lib-spigot/spigot-1.17.1.jar:ServerPythonInterpreterPlugin/lib-common/jython-standalone-2.7.2.jar:ServerPythonInterpreterPlugin/lib-common/java-websocket-1.4.0.jar --source-path ServerPythonInterpreterPlugin/src-common -d ${tmp_folder} ServerPythonInterpreterPlugin/src-common/com/macuyiko/minecraftpyserver/MinecraftPyServerPlugin.java

jar --verbose --create --no-manifest --file python.jar  -C ServerPythonInterpreterPlugin plugin.yml -C ServerPythonInterpreterPlugin config.yml -C ServerPythonInterpreterPlugin python -C ServerPythonInterpreterPlugin lib-common -C ${tmp_folder} com

rm -rf ${tmp_folder}

