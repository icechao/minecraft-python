from mcapi import *


class Minecraft:
    def __init__(self):
        self.player = CmdPlayer()

    @staticmethod
    def create():
        return Minecraft()

    def getBlock(self, *args):
        """Get block (x,y,z) => id:int"""
        return getblock(x, y, z).type

    def setBlock(self, x, y, z, block):
        setblock(x, y, z, block)

    def setBlocks(self, x0, y0, z0, x1, y1, z1, block):
        xmin = int(min(x0, x1))
        xmax = int(max(x0, x1))
        ymin = int(min(y0, y1))
        ymax = int(max(y0, y1))
        zmin = int(min(z0, z1))
        zmax = int(max(z0, z1))

        for x in range(xmin, xmax + 1):
            for y in range(ymin, ymax + 1):
                for z in range(zmin, zmax + 1):
                    setblock(x, y, z, block)

    def postToChat(self, msg):
        yell(msg)


class CmdPlayer:
    """Methods for the host (Raspberry Pi) player"""

    def __init__(self, name=None):
        self.name = name

    def setName(self, name=None):
        self.name = name

    def setPos(self, x, y, z, whom=None):
        if whom != None:
            self.name = whom
        teleport(x, y, z, self.name)

    def getPos(self, whom=None):
        if whom != None:
            self.name = whom
        pos = location(player(self.name))

        return pos

    def setTilePos(self, x, y, z, whom=None):
        self.setPos(int(x), int(y), int(z), whom)

    def getTilePos(self, whom=None):
        pos = self.getPos(whom)
        pos.x = int(pos.x)
        pos.y = int(pos.y)
        pos.z = int(pos.z)

        return pos
