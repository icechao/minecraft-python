# Instruction

1. Compile spigot-*.jar by following the procedure: [https://www.spigotmc.org/wiki/buildtools/#linux](https://www.spigotmc.org/wiki/buildtools/#linux)
1. Copy the output file `spigot-*.jar` here.
